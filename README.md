## Домашнее задание №1. Разработка Retrieval-Based чат-бота (vo_HW)


25.02.2024
Добавлен ReRanker ранжирующий выдачу поисковой системы для вывода более релевантных ответов
-----------------

Подход retrieval-based (основанный на извлечении) в чат-ботах означает использование заранее подготовленной базы данных ответов и механизма поиска, чтобы выбирать наилучший ответ на основе входящего вопроса пользователя. Этот тип чат-ботов не генерирует новые ответы, а выбирает из заранее подготовленного набора ответов наиболее подходящий.

![title](pictures/Retrieval-based_chatbot.png)


Бот ведет диалог как главный герой сериала "Доктор Хаус".


Данные взяты с платформы Kaggle отсюда: https://www.kaggle.com/datasets/kunalbhar/house-md-transcripts/data

Данные на английском


## Реализация
Код для обучения/инференса модели  взят из материалов 1 недели с семинарских занятий.

В файле training.ipynb реализованы 2 модели: на базе TF-IDF и более сложная на основе BERT.

Обученная модель сохраняется во внешний файл для дальнейшего использования в web-сервисе на Flask


## Запуск web сервиса
1. Выполнить все яейки training.ipynb. В результате работы должна появиться папка "data" в каталоге проекта с тремя файлами: questions.csv, answers.csv и bert_search_engine.pkl
2. Выполнить

    <b>$python web-service.py</b>

3. введите текст вопроса с строке адреса как показано на рисунке

![title](pictures/web_app_running.png)

<font color="red">
Замечания:

Добрый день, у вас нет кривых обучения вашего бота, и не показано пример диалоа, можете видео сделать. Так же распиите все более подробно. И еще я не вижу инфренса, вы его делали, если есть покажите где ?
</font>

Доработки по замечаниям:

По поводу кривых обучения, был бы рад показать какой-нибудь график функции потерь, но в данной реализации никакой оптимизационной задачи не решается. 

Для демонстрации диалого дополнительно сделал модуль cmd_bot.py. Скрин диалога прикладываю.
![title](pictures/cmd_chat_screenshot.png)


Описание принципа работы:
- подготавливается корпус пар Вопрос-Ответ, соответствие один к одному, индексы массивов соврпадают. В блокноте training.ipynb они называются "questions" и "answers";
- далее все вопросы пропускаются через токенезатор (TF-IDF или BERT), на выходе имеем массив векторов для вопросов. Первом случае получаем размерность векторов 5024 (tfidf), во втором размерность использованной модели "bert-base-cased".
Все, на этом "обучение" закончено.

- при получении произвольного вопроса, его текст также токенезируется, и среди полученного ранеее массива векторов ищется ближайший с помощью функции косинусного расстояния.
- получив индекс этого ближайщего вопроса, выбираем соответствующий ему ответ из массива ответов, и возвращаем его пользователю.

Работу системы поиска (инференс) контролировал с помощью одного из вопросов из оригинального корпуса
![title](pictures/inference_control.png)
Как и должно быть, поиск выдает его же самого (т.к. вектора полностью совпадают), плюс несколько ближайших по косинусному рассоянию



