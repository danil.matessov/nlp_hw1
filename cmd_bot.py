from bot import BERTBot
from bot import BERTSearchEngine
import re

print("The chat to House M.D. starting, please wait...")
bbot = BERTBot()
print("Type _exit_ to terminate")
while True:
    query = input("Your message: ")
    if query == "_exit_":
        print("House:        ","See you!")
        break
    else:
        response = re.sub(r'[^\x00-\x7F]+',' ', bbot.get_response(query)).strip()
        print("House:        ",response)