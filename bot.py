import pickle
import os
from tqdm import tqdm
import torch
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd
import torch

class BERTSearchEngine:
    def __init__(self, model, tokenizer, text_database):
        self.raw_procesed_data = [self.preprocess(sample, tokenizer) for sample in text_database]
        self.base = []
        self.retriever = None
        self.inverted_index = {}
        self._init_retriever(model, tokenizer, text_database)
        self._init_inverted_index(text_database)

    @staticmethod
    def preprocess(sentence: str, tokenizer):
        return tokenizer(sentence, padding=True, truncation=True, return_tensors='pt')

    def _embed_bert_cls(self, tokenized_text: dict[torch.Tensor]) -> np.array:
        with torch.no_grad():
            model_output = self.retriever(**{k: v.to(self.retriever.device) for k, v in tokenized_text.items()})
        embeddings = model_output.last_hidden_state[:, 0, :]
        embeddings = torch.nn.functional.normalize(embeddings)
        return embeddings[0].cpu().numpy()

    def _init_retriever(self, model, tokenizer, text_database):
        self.retriever = model
        self.tokenizer = tokenizer
        self.base = np.array([self._embed_bert_cls(self.preprocess(text, tokenizer)) for text in tqdm(text_database)])

    def retrieve(self, query: str) -> np.array:
        return self._embed_bert_cls(self.preprocess(query, self.tokenizer))

    def retrieve_documents(self, query: str, top_k=3) -> list[int]:
        query_vector = self.retrieve(query)
        cosine_similarities = cosine_similarity([query_vector], self.base).flatten()
        relevant_indices = np.argsort(cosine_similarities, axis=0)[::-1][:top_k]
        return relevant_indices.tolist()

    def _init_inverted_index(self, text_database: list[str]):
        self.inverted_index = dict(enumerate(text_database))

    def display_relevant_docs(self, query, full_database, top_k=3) -> list[int]:
        docs_indexes = self.retrieve_documents(query, top_k=top_k)
        return [str(self.inverted_index[ind]) for ind in docs_indexes]

class BERTBot:
    def __init__(self):
        self.questions = pd.read_csv(os.path.join('model','questions.csv'))
        self.answers = pd.read_csv(os.path.join('model','answers.csv'))
        with open(os.path.join('model','bert_search_engine.pkl'), 'rb') as pkl_file:
            self.bert_search_engine = pickle.load(pkl_file)

    def get_response(self, query):
        search_results = self.bert_search_engine.retrieve_documents(query, top_k=1)
        return self.answers.loc[search_results[0]]['line']


