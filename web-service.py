from flask import Flask
from bot import BERTBot
from bot import BERTSearchEngine


bbot = BERTBot()

app = Flask(__name__)

@app.route("/<query>")
def house(query):
    return bbot.get_response(query)

if __name__ == '__main__':
    app.run(port=5000, debug=True)